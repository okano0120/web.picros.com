# Picros README


## zyunbi

npm wo insuto-ru suru komando

```bash
npm install
```

gulp wo tukaeruyouni suru komando

```bash
npm install gulp-cli -g
```

##ugokasi kata

ro-karu sa-ba- de kidou

```bash
gulp
```

html to css wo tukurinaosu

```bash
gulp build:dev
```

dist no htlm to css wo tukurinaosu

```bash
gulp build:dist
```

- [demo](http://web.picros.com/)

## Credits
- special credits to the awesome images from [Unsplash](https://unsplash.com/)
- superb illustrations from [Open Doodles](https://www.opendoodles.com/)
- gallery lightbox from [Lokesh Dhakar](https://lokeshdhakar.com/projects/lightbox2/)
- awesome icons from [Fontawesome](https://fontawesome.com/)
- best CSS framework [Bootstrap](https://getbootstrap.com/)
- automation gurus from [Gulp](https://gulpjs.com/)
- awesome [Sass](https://sass-lang.com/)
- best sticky navbar plugin [Headroom](https://wicky.nillia.ms/headroom.js/)
- code highlighting [Prism.js](https://prismjs.com/)

#okano push
this is okano push test
